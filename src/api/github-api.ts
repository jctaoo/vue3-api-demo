interface IGithubApi {
  searchRepo(text: string): Promise<GithubRepo[]>
}

export interface GithubRepo {
  name: string
  description: string
  link: string
}

class GithubNetApi implements IGithubApi {
  static readonly api = new GithubNetApi();

  async searchRepo(text: string): Promise<GithubRepo[]> {
    const raw = await fetch(`https://api.github.com/search/repositories?q=${text}`)
    const data = await raw.json()
    return data.items.map((repo: any) => {
      return { 
        name: repo.full_name, 
        description: repo.description,
        link: `https://github.com/${repo.full_name}`
      }
    });
  }
}

class GithubMockApi implements IGithubApi {
  static readonly api = new GithubMockApi();

  async searchRepo(text: string): Promise<GithubRepo[]> {
    return [
      { name: "Hello Mock Api", description: "from mock api", link: "github.com" }
    ];
  }
}

export enum Mode {
  mock,
  net
}

export const githubApi = (mode: Mode = Mode.net): IGithubApi => {
  return mode == Mode.mock ? GithubMockApi.api : GithubNetApi.api;
}